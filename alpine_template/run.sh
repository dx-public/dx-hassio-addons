#!/bin/bash
set -e

CONFIG_PATH=/data/options.json
ARCH=$(cat /etc/apk/arch);
. /etc/os-release;

echo "$ID (${ARCH}) version : $VERSION_ID";

if ! grep -q "/edge/" /etc/apk/repositories; then 
echo "First Time: Upgrade all $ID $ARCH pkgs to edge"
echo "https://dl-cdn.alpinelinux.org/alpine/edge/main" > /etc/apk/repositories
echo "https://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
echo "firsttime" > /etc/.first
fi

echo "Checking if online/offline"
if curl -sI --fail https://dl-cdn.alpinelinux.org -o /dev/null; then
 echo "ONLINE..."
 online="online"
 echo "Syncing current packages with mirror packages"
 apk update
 pkgupdates=$(apk -s upgrade | awk 'BEGIN{count=-1} //{count++} END{print count}')
 echo "Updating $pkgupdates packages"
 #apk add --upgrade apk-tools bash curl --in Dockerfile--
 if [ -f /etc/.first ]; then echo "Refreshing updated packages"; apk upgrade --available; rm /etc/.first; fi
 if [ ! -f /etc/.first ]; then echo "Updating current packages"; apk upgrade; fi
 sync
else
 echo "OFFLINE..."
 online="offline"
fi

readarray -t RUN_SCRIPT < <(jq --raw-output '.run_script[]' $CONFIG_PATH)
readarray -t ENV_VARS < <(jq --raw-output '.env_vars[]' $CONFIG_PATH)

#echo "Running: ${ENV_VARS[*]} ${RUN_SCRIPT[*]}"
#exec env "${ENV_VARS[*]}" ${RUN_SCRIPT[*]}

ENV_VARS_CMDS=""
if [ ! -z "$ENV_VARS" ]; then ENV_VARS_CMDS=$(printf "%s; " "${ENV_VARS[@]}"); fi
RUN_SCRIPT_CMDS=$(printf "%s; " "${RUN_SCRIPT[@]}")

echo "Running: ${ENV_VARS_CMDS} ${RUN_SCRIPT_CMDS}"
bash -c "${ENV_VARS_CMDS} ${RUN_SCRIPT_CMDS}"


echo "$ID (${ARCH}) version : $VERSION_ID was $online when started. There were $pkgupdates updated packages on startup. Please rebuild system while online to force updated persistent packages so the packages will be applied on base image."
