#!/bin/bash
set -e

CONFIG_PATH=/data/options.json
FPHP_SHARE_PATH="/share/frankenphp"
FPHP_PATH=/etc/frankenphp/frankenphp

XDG_CONFIG_HOME=/share/caddy
XDG_DATA_HOME=/share/caddy

if [ ! -d /etc/frankenphp ]; then mkdir -p /etc/frankenphp; fi
if [ ! -f /etc/frankenphp/Caddyfile ]; then
echo "creating Caddyfile"
echo ":80 {" > /etc/frankenphp/Caddyfile
echo "	root * /etc/frankenphp/www" >> /etc/frankenphp/Caddyfile
echo "	file_server" >> /etc/frankenphp/Caddyfile
echo "}" >> /etc/frankenphp/Caddyfile
fi

if [ ! -d /etc/frankenphp/www ]; then mkdir -p /etc/frankenphp/www; fi
if [ ! -f /etc/frankenphp/www/index.html ]; then
echo "<html><body>It works!</body></html>" > /etc/frankenphp/www/index.html
fi

if [ -f "$FPHP_SHARE_PATH/Caddyfile" ]; then
echo "found custom Caddyfile"
CADDYFILE="$FPHP_SHARE_PATH/Caddyfile"
else
CADDYFILE="/etc/frankenphp/Caddyfile"
fi

DEFAULT_ARGS=( "run --config" "$CADDYFILE" )

readarray -t FLAGS < <(jq --raw-output '.flags[]' $CONFIG_PATH)
readarray -t ENV_VARS < <(jq --raw-output '.env_vars[]' $CONFIG_PATH)

if [ -f "$FPHP_SHARE_PATH/frankenphp.bin" ]; then
    FPHP_PATH="$FPHP_SHARE_PATH/frankenphp.bin"
    echo "Found custom FrankenPHP: $($FPHP_PATH version)"
else
    echo "Using built-in FrankenPHP: $($FPHP_PATH version)"
    #echo "formatting caddyfile: $($FPHP_PATH fmt --overwrite $CADDYFILE)
fi

echo "Fixing Config File"
#$FPHP_PATH fmt --overwrite "$CADDYFILE"

echo "Running FrankenPHP: ${ENV_VARS[*]} $FPHP_PATH ${DEFAULT_ARGS[*]} ${FLAGS[*]}"
exec env -S "${ENV_VARS[*]}" $FPHP_PATH ${DEFAULT_ARGS[*]} ${FLAGS[*]}
