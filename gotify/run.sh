#!/bin/bash
set -e

GOTIFY_SHARE_PATH="/config/gotify"
CONFIG_PATH="$GOTIFY_SHARE_PATH/config.yml"
GOTIFY_PATH=/config/gotify/gotify

#GOTIFY_SERVER_PORT=80
#GOTIFY_SERVER_KEEPALIVEPERIODSECONDS=0
#GOTIFY_SERVER_LISTENADDR=
#GOTIFY_SERVER_SSL_ENABLED=false
#GOTIFY_SERVER_SSL_REDIRECTTOHTTPS=true
#GOTIFY_SERVER_SSL_LISTENADDR=
#GOTIFY_SERVER_SSL_PORT=443
#GOTIFY_SERVER_SSL_CERTFILE=
#GOTIFY_SERVER_SSL_CERTKEY=
#GOTIFY_SERVER_SSL_LETSENCRYPT_ENABLED=false
#GOTIFY_SERVER_SSL_LETSENCRYPT_ACCEPTTOS=false
#GOTIFY_SERVER_SSL_LETSENCRYPT_CACHE=certs
## GOTIFY_SERVER_SSL_LETSENCRYPT_HOSTS=[mydomain.tld, myotherdomain.tld]
## GOTIFY_SERVER_RESPONSEHEADERS={X-Custom-Header: "custom value", x-other: value}
## GOTIFY_SERVER_TRUSTEDPROXIES=[127.0.0.1,192.168.178.2/24]
## GOTIFY_SERVER_CORS_ALLOWORIGINS=[.+\.example\.com, otherdomain\.com]
## GOTIFY_SERVER_CORS_ALLOWMETHODS=[GET, POST]
## GOTIFY_SERVER_CORS_ALLOWHEADERS=[X-Gotify-Key, Authorization]
## GOTIFY_SERVER_STREAM_ALLOWEDORIGINS=[.+.example\.com, otherdomain\.com]
#GOTIFY_SERVER_STREAM_PINGPERIODSECONDS=45
#GOTIFY_DATABASE_DIALECT=sqlite3
#GOTIFY_DATABASE_CONNECTION=data/gotify.db
#GOTIFY_DEFAULTUSER_NAME=admin
#GOTIFY_DEFAULTUSER_PASS=admin
#GOTIFY_PASSSTRENGTH=10
#GOTIFY_UPLOADEDIMAGESDIR=data/images
#GOTIFY_PLUGINSDIR=data/plugins
#GOTIFY_REGISTRATION=false

DEFAULT_ARGS=( "" )
readarray -t FLAGS < <(jq --raw-output '.flags[]' $CONFIG_PATH)
readarray -t ENV_VARS < <(jq --raw-output '.env_vars[]' $CONFIG_PATH)

if [ -f "$GOTIFY_SHARE_PATH/gotify" ]; then
    GOTIFY_PATH="$GOTIFY_SHARE_PATH/gotify"
    echo "Running custom at $(echo $GOTIFY_PATH)"
else
    echo "Using built-in: $(echo $GOTIFY_PATH)"
fi

echo "config found at https://raw.githubusercontent.com/gotify/server/master/config.example.yml"

#echo "Fixing Config File"
#$GOTIFY_PATH fmt --overwrite "$GOTIFY_SHARE_PATH/config.yml"

echo "Running Gotify: ${ENV_VARS[*]} $GOTIFY_PATH ${DEFAULT_ARGS[*]} ${FLAGS[*]}"
exec env -S "${ENV_VARS[*]}" $GOTIFY_PATH ${DEFAULT_ARGS[*]} ${FLAGS[*]}