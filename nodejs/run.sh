#!/bin/bash
set -e

CONFIG_PATH=/data/options.json
NODEJS_SHARE_PATH="/share/node"
NODEJS_PATH=$(which node 2>/dev/null)

DEFAULT_ARGS=""
readarray -t FLAGS < <(jq --raw-output '.flags[]' $CONFIG_PATH)
readarray -t ENV_VARS < <(jq --raw-output '.env_vars[]' $CONFIG_PATH)

echo "**Flags should contain the index.js in /config or in /share**. Serve port via js file "
echo "Running NODEJS: ${ENV_VARS[*]} $NODEJS_PATH ${DEFAULT_ARGS[*]} ${FLAGS[*]}"
echo "Version of NODEJS: $($NODEJS_PATH -v)"
exec env "${ENV_VARS[*]}" $NODEJS_PATH ${DEFAULT_ARGS[*]} ${FLAGS[*]}
