#!/bin/bash
set -e
PROG='stalwart'
VERSION_ARGS='--version'
START_ARGS="-c /config/$PROG/etc/config.toml"

mkdir -p /config/${PROG}

if [ -f "/config/${PROG}/${PROG}.bin" ]; then
    BIN_PATH="/config/${PROG}/${PROG}.bin"
    chmod u+x $BIN_PATH
    echo "Found custom ${PROG} bin: $($BIN_PATH $VERSION_ARGS)"
else
    mkdir -p /etc/${PROG}
    cd /etc/${PROG}
    curl -L https://github.com/stalwartlabs/mail-server/releases/download/v0.10.7/stalwart-mail-x86_64-unknown-linux-musl.tar.gz -o /etc/${PROG}/${PROG}.tar.gz
    tar -xvf /etc/${PROG}/${PROG}.tar.gz
    mv /etc/${PROG}/${PROG}-mail /etc/${PROG}/${PROG}
    BIN_PATH="/etc/${PROG}/${PROG}"
    echo "Using built-in ${PROG} bin: $($BIN_PATH $VERSION_ARGS)"
fi

if [ ! -f "/config/${PROG}/etc/config.toml" ]; then
echo "Please write down this password. It will not be shown again"
$($BIN_PATH --init /config/${PROG})
fi

$($BIN_PATH $START_ARGS)
/bin/sh