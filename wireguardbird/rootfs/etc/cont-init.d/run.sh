#!/bin/bash
set -e

if [ ! -d /ssl/bird ]; then mkdir -p /ssl/bird; fi
if [ ! -f /ssl/bird/bird.conf ]; then
cat >/ssl/bird/bird.conf<<'EOF'
log syslog all;
debug protocols all;
router id 172.27.66.1;
protocol device {
}
EOF
fi
cp /ssl/bird/bird.conf /etc/bird.conf

if [ ! -d /ssl/wireguard ]; then mkdir -p /ssl/wireguard; fi
if [ ! -f /ssl/wireguard/wg0.conf ]; then
cat >/ssl/wireguard/wg0.conf<<'EOF'
[Interface]
Address    = 172.27.66.1/32
PrivateKey = <PrivateKey>
#PublicKey = <PublicKey>
Listenport = 51820
Mtu        = 1420
Table      = Off

PostUp     = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -A FORWARD -o wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
PostDown   ="iptables -D FORWARD -i wg0 -j ACCEPT; iptables -D FORWARD -o wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE

#[Peer]
#PublicKey    = <public key of peer>
#PresharedKey = <PSK of all clients>
#AllowedIPs   = 0.0.0.0/0, ::/0

#Endpoint     = <Endpoint DNS or IP of peer>:51820
#PersistentKeepalive = 30
EOF
echo "Please update wg0.conf in wireguard directory and restart the container"
exit
fi
cp /ssl/wireguard/wg0.conf /etc/wireguard/wg0.conf

echo "Running Bird"
/usr/sbin/bird
echo "Running Wireguard"
wg-quick down wg0 || true
wg-quick up wg0

if [[ $(</proc/sys/net/ipv4/ip_forward) -eq 0 ]]; then
    bashio::log.warning
    bashio::log.warning "IP forwarding is disabled on the host system!"
    bashio::log.warning "You can still use WireGuard to access Hass.io,"
    bashio::log.warning "however, you cannot access your home network or"
    bashio::log.warning "the internet via the VPN tunnel."
    bashio::log.warning
    bashio::log.warning "Please consult the add-on documentation on how"
    bashio::log.warning "to resolve this."
    bashio::log.warning
fi

#sleep 2
#iptables -L -v -n
#sleep 2
#ip addr
sleep 30
wg show
/bin/sh