#!/bin/bash
set -e

if [ ! -d /ssl/bird ]; then mkdir -p /ssl/bird; fi
if [ ! -f /ssl/bird/bird.conf ]; then
cat >/ssl/bird/bird.conf<<'EOF'
log syslog all;
debug protocols all;
router id 172.27.66.1;
protocol device {
}
EOF
fi
cp /ssl/bird/bird.conf /etc/bird/bird.conf

if [ ! -d /ssl/wireguard ]; then mkdir -p /ssl/wireguard; fi
if [ ! -f /ssl/wireguard/wg0.conf ]; then
cat >/ssl/wireguard/wg0.conf<<'EOF'
[Interface]
Address    = 172.27.66.1/32
PrivateKey = <PrivateKey>
#PublicKey = <PublicKey>
Listenport = 31820
Mtu        = 1420
Table      = Off

#[Peer]
#PublicKey    = <public key of peer>
#PresharedKey = <PSK of all clients>
#AllowedIPs   = 0.0.0.0/0, ::/0

#Endpoint     = <Endpoint DNS or IP of peer>:51820
#PersistentKeepalive = 30
EOF
echo "Please update wg0.conf in wireguard directory and restart the container"
exit
fi
cp /ssl/wireguard/wg0.conf /etc/wiregaurd/wg0.conf

echo "Running Bird and Wireguard"
bird
wg-quick up wg0
/bin/sh